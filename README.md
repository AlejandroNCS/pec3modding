# PEC3 - Modding y creación de niveles 

# Atención!!
En el video el botón de pasar de nivel esta activo para poder avanzar más rapido por los niveles para la demostración, en el Ejecutable del juego el botón solo será activo en el momento en el que el nivel este completado.

# Desarrollar un clon de Sokoban en Unity
Implementar en 2D un clon básico de Sokoban con las mecánicas básicas del juego original: Movimiento de personaje, poder empujar cajas en una cuadrícula, y contar con casillas “objetivo” donde las cajas deben colocarse. El prototipo debe componerse de 10 niveles de dificultad creciente y un botón o tecla para reiniciar el nivel. Superar un nivel cargará el siguiente nivel.

# Solución
Se ha creado un fichero de texto con números, asignando a cada número un prefab distinto. Luego en el script se instancia los prefabs en las posiciones determinadas por el fichero de texto.

# Video a Youtube
[Enlace a video](https://www.youtube.com/watch?v=wG5icxP96Z0)