using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public BuilderLevels levelBuilder;
    public GameObject nextButton;
    private bool readyForInput;
    private Player player;
    private float contador = 0f;
    private int nivel = 1;
    public TextMeshProUGUI contadorLabel;
    public TextMeshProUGUI nivelLabel;
    public TextMeshProUGUI finJuego;
    public Button Reiniciar;
    public TextMeshProUGUI numMovTexto;

    void Start() {
        finJuego.gameObject.SetActive(false);
        Reiniciar.gameObject.SetActive(false);
        nextButton.SetActive(false);
        ResetScene();
    }

    void Update() {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input.Normalize();
        Reiniciar.gameObject.SetActive(true);
        if (input.sqrMagnitude > 0.5) {
            if (readyForInput) {
                readyForInput = false;
                player.Move(input);
               nextButton.SetActive(IsLevelComplete());
            }
        }
        else {
            readyForInput = true;
        }
        contador += Time.deltaTime;
        contadorLabel.text = "Tiempo: " + Mathf.Floor(contador).ToString()+" s";
        nivelLabel.text = "Nivel: "+ nivel.ToString();
        if(nivel == 11)
        {
            nivel = 1;
            finJuego.gameObject.SetActive(true);
            StartCoroutine(EsperarDosSegundos());
        }

        numMovTexto.text = "Movimientos: " + Player.numMov;
    }

    IEnumerator EsperarDosSegundos()
    {
        yield return new WaitForSeconds(2f);
        finJuego.gameObject.SetActive(false);
    }

    public void NextLevel() {
        nextButton.SetActive(false);
        levelBuilder.NextLevel();
        reiniciarContador();
        nivel++;
        StartCoroutine(ResetSceneAsync());
        
    }

    public void ResetScene() {
        reiniciarContador();
        StartCoroutine(ResetSceneAsync());
        
    }

    bool IsLevelComplete() {
        Box[] boxes = FindObjectsOfType<Box>();
        foreach (var box in boxes) {
            if (!box.onCrox) return false;
        }
        return true;
    }

    IEnumerator ResetSceneAsync() {
        if (SceneManager.sceneCount > 1) {
            AsyncOperation async = SceneManager.UnloadSceneAsync("LevelScene");
            while (!async.isDone) {
                yield return null;
                Debug.Log("Unloading");
            }
            Debug.Log("Unloading done.");
            Resources.UnloadUnusedAssets();
        }

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("LevelScene", LoadSceneMode.Additive);
        while (!asyncLoad.isDone) {
            yield return null;
            Debug.Log("Loading");
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("LevelScene"));
        levelBuilder.Build();
        player = FindObjectOfType<Player>();
        Debug.Log("Level loaded.");
    }
    
    private void reiniciarContador()
    {
        contador = 0f;
        Player.numMov = 0;
    }

    public void reiniciarNivel()
    {
        SceneManager.LoadScene(0);
    }

    public void Salir()
    {
        Application.Quit();
    }
    
}
